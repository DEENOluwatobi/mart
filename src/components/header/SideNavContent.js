import React from 'react'
import { KeyboardArrowRight } from '@mui/icons-material';

const SideNavContent = ({title, one, two, three}) => {
  return (
    <div className='py-3 border-b-[1px] border-b-gray-300'>
        <h3 className='text-[1em] font-titleFont font-semibold mb-1 px-6'>
            {title}
        </h3>
        <ul>
            <li className='flex items-center justify-between hover:bg-zinc-200 px-6 py-1 cursor-pointer text-[.8em]'>
                {one} 
                <span>
                    <KeyboardArrowRight/>
                </span>
            </li>
            <li className='flex items-center justify-between hover:bg-zinc-200 px-6 py-1 cursor-pointer text-[.8em]'>
                {two} 
                <span>
                    <KeyboardArrowRight/>
                </span>
            </li>
            <li className='flex items-center justify-between hover:bg-zinc-200 px-6 py-1 cursor-pointer text-[.8em]'>
                {three} 
                <span>
                    <KeyboardArrowRight/>
                </span>
            </li>
        </ul>
    </div>
  )
}

export default SideNavContent