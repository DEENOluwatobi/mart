import React, {useState} from 'react'
import MenuIcon from '@mui/icons-material/Menu';
import SideNavContent from './SideNavContent';
import { AccountCircle, Close } from '@mui/icons-material';
import {motion} from "framer-motion";
import ThemeBtn from './ThemeBtn';
import { useSelector } from 'react-redux';
import { Link } from 'react-router-dom';

const HeaderBottom = () => {
  const [ sideBar, setSideBar ] = useState(false);
  const userInfo = useSelector((state) => state.BBReducer.userInfo);

  return (
    <div className='w-full px-4 h-7 bg-mart_light text-white flex items-center'>
        {/* =========== listItems Starts here =========== */}
            <ul className='flex items-center gap-5 tracking-wide'>
                <li 
                onClick={()=>setSideBar(!sideBar)}
                className='headerBtm flex justify-center items-center gap-1'
                >
                    <MenuIcon/>
                    All
                </li>
                <li className='headerBtm hidden mdl:inline-flex'>
                    Today's deal
                </li>
                <li className='headerBtm hidden mdl:inline-flex'>
                    Customer Service
                </li>
                <li className='headerBtm hidden mdl:inline-flex'>
                    Gift Cards
                </li>
                <li className='headerBtm hidden mdl:inline-flex'>
                    Registry
                </li>
                <Link to='/sell' className='headerBtm hidden mdl:inline-flex'>
                    Sell
                </Link>
            </ul>
        {/* =========== listItems ends here =========== */}

        {/* =========== Theme ends here =========== */}
            <div className='flex ml-auto'>
                <ThemeBtn/>
            </div>
        {/* =========== Theme ends here =========== */}

        {/* =========== sideNav Starts here =========== */}
            {
                sideBar && (
                    <div 
                    onClick={()=>setSideBar(!sideBar)}
                    className='w-full h-screen text-black fixed top-0 left-0 bg-mart_blue bg-opacity-50 z-50'>
                        <div className='w-full h-full relative'>
                            <motion.div 
                            initial={{x: -500, opacity: 0}}
                            animate={{x: 0, opacity: 1}}
                            transition={{duration: 0.3}}
                            className='w-[250px] mdl:w-[350px] h-full bg-white border border-black'
                            >
                                {
                                    userInfo ? (
                                        <div className='w-full bg-mart_light text-white py-2 px-6 flex items-center gap-4'>
                                            <div className='w-8 h-8 rounded-full border-[1px] border-mart_yellow'>
                                                <img src={userInfo.image} alt='' className='object-cover'/>
                                            </div>
                                            <h3 className='font-titleFont font-sembold text-sm tracking-wide'>Hello, {userInfo.userName}</h3>
                                        </div>
                                    ) : (
                                        <div className='w-full bg-mart_light text-white py-2 px-6 flex items-center gap-4'>
                                            <AccountCircle/>
                                            <h3 className='font-titleFont font-sembold text-sm tracking-wide'>Hello, Sign in</h3>
                                        </div>
                                    )
                                }
                                
                                <SideNavContent 
                                    title="Digital Content & Devices"
                                    one="BestBuy Music"
                                    two="Kindle E-readers & Books"
                                    three="App Store"
                                />
                                <SideNavContent 
                                    title="Shop By Department"
                                    one="Electronics"
                                    two="Computers"
                                    three="Smart Home"
                                />
                                <SideNavContent 
                                    title="Programs & Features"
                                    one="Gift Cards"
                                    two="BestBuy live"
                                    three="International Shopping"
                                />
                                <SideNavContent 
                                    title="Help & Settings"
                                    one="Your Account"
                                    two="Customer Service"
                                    three="Contact Us"
                                />

                                <span 
                                onClick={()=>setSideBar(false)}
                                className='cursor-pointer absolute top-0 left-[260px] mdl:left-[360px] w-10 h-10 text-red-500 flex items-center justify-center border bg-gray-200 hover:bg-red-500 hover:text-white duration-300 rounded-full'>
                                    <Close/>
                                </span>
                            </motion.div>
                        </div>
                    </div>
                )
            }
        {/* =========== sideNav ends here =========== */}
    </div>
  )
}

export default HeaderBottom