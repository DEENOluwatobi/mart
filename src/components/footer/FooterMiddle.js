import React from 'react'
import FooterMiddleList from './FooterMiddleList'
import { middleList } from '../../constants'
import {Nig} from "../../assets/Images/index"

const FooterMiddle = () => {
  return (
    <div className='w-full bg-mart_light text-white'>
        {/* ============ Top starts here ============ */}
            <div className='w-full border-b-[1px] border-gray-500 py-10'>
                <div className='max-w-[80%] lgl:max-w-4xl xl:max-w-5xl mx-auto text-gray-300'>
                    <div className='w-full grid-cols-2 lg:grid-cols-4 grid place-items-start center gap-2'>
                        {
                            middleList.map((item)=>(
                                <FooterMiddleList 
                                    key={item._id}
                                    title={item.title}
                                    listItem={item.listItem}
                                /> 
                            ))
                        }
                    </div>
                </div>
            </div>
        {/* ============ Top ends here ============ */}

        {/* ============ Bottom starts here ============ */}
            <div className='w-full flex gap-4 items-center justify-center py-6'>
                <div className='text-[#b4b5b7] font-semibold text-xl font-titleFont cursor-pointer'>
                    Big<span className='font-normal text-mart_yellow'>Boss</span>
                </div>

                <div className='flex gap-2'>
                    <p className='flex gap-1 items-center justify-center border border-gray-500 hover:border-mart_yellow cursor-pointer duration-200 px-2 py-1'>
                        English
                    </p>
                </div>

                <div className='flex gap-2'>
                    <p className='flex gap-2 items-center justify-center border border-gray-500 hover:border-mart_yellow cursor-pointer duration-200 px-2 py-1'>
                        <img src={Nig} alt='' className='w-6 h-6 rounded-full object-cover'/>
                        Nigeria
                    </p>
                </div>
            </div>
        {/* ============ Bottom ends here ============ */}
    </div>
  )
}

export default FooterMiddle