import React from 'react'
import { bottomList } from '../../constants'

const FooterBottom = () => {
  return (
    <div className='w-full bg-footerBottom py-6'>
        <div className='max-w-[80%] xl:max-w-5xl mx-auto'>
            <div className='w-full grid grid-cols-3 md:grid-cols-5 xl:grid-cols-7 gap-3 place-content-center '>
                {
                    bottomList.map((item) => (
                        <div key={item._id} className='group'>
                            <h3 className='footerBottomTitle'>{item.title}</h3>
                            <p className='footerBottomText'>{item.des}</p>
                        </div>
                    ))
                }
            </div>
        </div>
    </div>
  )
}

export default FooterBottom