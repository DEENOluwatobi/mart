import React from 'react'

const FooterMiddleList = ({title, listItem}) => {
  return (
    <div>
        <h3 className='font-titleFont text-mart_yellow text-[12px] lg:text-base font-semibold mb-3'>
            {title}
        </h3>
        <ul className='flex flex-col gap-1 lg:gap-3 font-bodyFont'>
            {
                listItem.map((item) => item.listData.map((data, index) => (
                    <li key={index} className='footer-link'>{data}</li>
                )))
            }
        </ul>
    </div>
  )
}

export default FooterMiddleList