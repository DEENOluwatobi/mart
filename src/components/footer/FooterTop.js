import React from 'react';
import { useTheme } from '../../context/ThemeContext';
import { Link } from 'react-router-dom';
import { useSelector } from 'react-redux';

const FooterTop = () => {
    const { theme } = useTheme();
    const userInfo = useSelector((state) => state.BBReducer.userInfo);

  return (
    <div>
        {
            userInfo ? (
                <div>
                    
                </div>
            ) : (
                <div className={`${theme === 'dark' ? 'bg-mart_blue' : 'bg-white'} w-full py-6 border-t-[1px] border-b-[1px]`}>
                    <div className='w-full  px-8'>
                        <div className='w-64 mx-auto text-center flex flex-col gap-1'>
                            <p className={`${theme === 'dark' ? 'text-gray-300' : 'text-black'} text-sm`}>See Personalized Recommendations</p>
                            <Link to='/signin' className='w-full bg-yellow-400 rounded-md py-1 font-semibold cursor-pointer hover:bg-yellow-500 active:bg-yellow-600'>
                                Sign in
                            </Link>
                            <p className={`${theme === 'dark' ? 'text-gray-300' : 'text-black'} text-xs mt-1`}>
                                New Customer?{" "}
                                <Link to='/signup' className='text-blue-600 ml-1 cursor-pointer'>
                                    Start here.
                                </Link>
                            </p>
                        </div>
                    </div> 
                </div>
            )
        }
    </div>
  )
}

export default FooterTop