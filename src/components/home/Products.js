import React, { useState } from 'react';
import { useLoaderData } from "react-router-dom";
import { useDispatch } from 'react-redux';
import StarIcon from "@mui/icons-material/Star";
import ApiIcon from "@mui/icons-material/Api";
import ShoppingCartIcon from "@mui/icons-material/ShoppingCart";
import ArrowCircleRightIcon from "@mui/icons-material/ArrowCircleRight";
import FavoriteIcon from "@mui/icons-material/Favorite";
import { addToCart } from '../../redux/BBSlice';
import { useTheme } from '../../context/ThemeContext';
import { motion } from 'framer-motion';
import { Close } from '@mui/icons-material';


const Products = () => {

    const { theme } = useTheme();
    const dispatch = useDispatch();
    const data = useLoaderData();
    const productData = data.data;
    const [selectedProduct, setSelectedProduct] = useState(null);
    const handleClick = (itemId) => {
        setSelectedProduct(itemId);
    };

    return (
        <div className='max-w-screen-xl lg:max-w-[85%] xl:max-w-screen-2xl mx-auto grid grid-cols-2 md:grid-cols-3 xl:grid-cols-4 gap-4 md:gap-6 xl:gap-10 px-4 py-6'>
            {
                productData.map((item) => (
                    <div>
                        <div key={item.id} className={`${ theme === 'dark' ? 'bg-mart_blue border-black hover:shadow-gray-400' : 'bg-white border-gray-200 hover:shadow-testShadow'} h-auto border-[1px] rounded-md py-6 z-30 hover:border-transparent 
                        shadow-none duration-200 relative group flex flex-col gap-4`}
                        >
                            <span className=' text-[.2em] md:text-[.7em] capitalize italic text-gray-500 absolute top-0 right-2'>
                                {item.category}
                            </span>

                            <div className='w-full h-auto flex items-center justify-center relative '>
                                <div className={`${theme === 'dark' ? 'bg-white' : ''} w-full flex justify-center items-center`}>
                                    <img 
                                        src={item.image} 
                                        alt='ProductImage' 
                                        className='w-16 h-24 md:w-24 md:h-36 xl:w-48 xl:h-52 object-contain'
                                    />
                                </div>
                                <ul 
                                    className={`${theme === 'dark' ? 'bg-mart_light border-r-black' : 'bg-gray-100'} w-full h-28 md:h-32  absolute -bottom-[150px] opacity-0 group-hover:opacity-100 group-hover:-bottom-3 md:group-hover:-bottom-4 duration-700 flex
                                    flex-col items-end justify-center gap-1 md:gap-2 font-titleFont px-2 border-1 border-r overflow-hidden`}
                                >
                                    <li className={`productLi ${theme === 'dark' ? 'text-gray-300 hover:text-mart_yellow hover:border-b-mart_yellow ' : 'text-gray-600 hover:text-black hover:border-b-gray-700'}`}>
                                        Compare{" "}
                                        <span><ApiIcon className='text-blue-500'/></span>
                                    </li>

                                    <li 
                                    className={`productLi ${theme === 'dark' ? 'text-gray-300 hover:text-mart_yellow hover:border-b-mart_yellow ' : 'text-gray-600 hover:text-black hover:border-b-gray-700'}`}
                                    onClick={() => dispatch( addToCart({
                                        id: item.id,
                                        title: item.title,
                                        description: item.description,
                                        price: item.price,
                                        category: item.category,
                                        image: item.image,
                                        quantity: 1,    
                                    }))}
                                    >
                                        Add to Cart{" "}
                                        <span><ShoppingCartIcon className='text-green-500'/></span>
                                    </li>

                                    <li 
                                    className={`productLi ${theme === 'dark' ? 'text-gray-300 hover:text-mart_yellow hover:border-b-mart_yellow ' : 'text-gray-600 hover:text-black hover:border-b-gray-700'}`}
                                    onClick={() => handleClick(item.id)}>
                                        View Details{" "}
                                        <span><ArrowCircleRightIcon className='text-yellow-500'/></span>
                                    </li>
                                    <li className={`productLi ${theme === 'dark' ? 'text-gray-300 hover:text-mart_yellow hover:border-b-mart_yellow ' : 'text-gray-600 hover:text-black hover:border-b-gray-700'}`}>
                                        Add to Wish List{" "}
                                        <span><FavoriteIcon className='text-red-500'/></span>
                                    </li>
                                </ul>
                            </div>
                            <div className={`${theme === 'dark' ? 'bg-mart_blue' : 'bg-white'} px-2 md:px-4 z-10`}>
                                <div className='flex justify-between items-center'>
                                    <h2 className={`${theme === 'dark' ? 'text-yellow-500' : 'text-mart_blue'} font-titleFont tracking-wide text-[.6em] mdl:text-[.9em] font-medium`}>
                                        {item.title.substring(0,20)}
                                    </h2>
                                    <p className={`${theme === 'dark' ? 'text-gray-300' : 'text-gray-600'} text-[.5em] mdl:text-sm font-semibold`}>
                                        ${item.price}
                                    </p>
                                </div>
                                <div>
                                    <p className={`${theme === 'dark' ? 'text-gray-400' : 'text-black'} text-[.5em] mdl:text-[.8em] mb-1 capitalize`}>
                                        {item.description.substring(0,80)}...
                                    </p>
                                    <div className='text-yellow-500'>
                                        <StarIcon className='w-10 h-10'/>
                                        <StarIcon className='w-10 h-10'/>
                                        <StarIcon className='w-10 h-10'/>
                                        <StarIcon className='w-10 h-10'/>
                                        <StarIcon className='w-10 h-10'/>
                                    </div>
                                </div>
                                <div>
                                    <button
                                        onClick={() => dispatch( addToCart({
                                            id: item.id,
                                            title: item.title,
                                            description: item.description,
                                            price: item.price,
                                            category: item.category,
                                            image: item.image,
                                            quantity: 1,    
                                        }))}
                                        className='w-full font-titleFont font-medium text-xs md:text-sm bg-gradient-to-tr from-yellow-400 
                                        to-yellow-200 border rounded-md mt-3
                                        hover:from-yellow-300 hover:to-yellow-200 border-yellow-500 hover:border-yellow-700 
                                        active:bg-gradient-to-bl active:from-yellow-400 active:to-yellow-500 duration-200 py-1 md:py-1.5'
                                    >
                                        Add to Cart
                                    </button>
                                </div>
                            </div>  
                        </div>


                        {/* ============== VIEW PRODUCT DETAILS MODAL ============== */}

                        {selectedProduct === item.id && (
                            <motion.div
                            className="fixed inset-0 z-40 overflow-auto bg-mart_blue bg-opacity-70 flex items-center justify-center"
                            >
                                <motion.div
                                 initial={{scale: 0.5, opacity: 0}}
                                 animate={{scale: 1, opacity: 1}} 
                                 exit={{scale: 0.5, opacity: 0}}
                                 className={`${theme === 'dark' ? 'bg-mart_blue text-gray-300 border border-mart_yellow' : 'bg-white'} relative p-6 m-4 max-w-3xl max-h-full  z-40 flex- flex-col rounded-md`}>
                                    <div className='grid grid-cols-4 justify-start items-start w-full gap-3'>
                                        <div className='w-full h-full col-span-1'>
                                            <img 
                                                src={item.image} 
                                                alt="product-img" 
                                                className={`${theme === 'dark' ? 'border border-mart_yellow rounded-md bg-white' : 'bg-white'} w-full h-full object-contain`}
                                            />
                                        </div>

                                        <div className='w-full col-span-3 flex flex-col gap-1'>
                                            <h2 className={`${theme === 'dark' ? 'text-yellow-600' : 'text-black'} font-semibold text-base mb-2`}>{item.title}</h2>
                                            <p className={`${theme === 'dark' ? 'text-gray-400' : 'text-black'} capitalize pr-10 text-[.85em] border-b-[1px] border-b-gray-300 pb-1`}>
                                                {item.description}
                                            </p>
                                            <span className={`${theme === 'dark' ? 'text-white' : 'text-black'} text-sm capitalize`}>
                                                <span className='font-semibold'>Category: {" "}</span>
                                                {item.category}
                                            </span>
                                            
                                            <p className={`${theme === 'dark' ? 'text-white' : 'text-black'} text-sm mt-1 font-semibold`}>Unit price: {" "}
                                                <span className={`${theme === 'dark' ? 'text-white' : 'text-black'} font-normal`}>${item.price}</span>
                                            </p>
                                            
                                            <div className='text-yellow-500'>
                                                <StarIcon className='w-10 h-10'/>
                                                <StarIcon className='w-10 h-10'/>
                                                <StarIcon className='w-10 h-10'/>
                                                <StarIcon className='w-10 h-10'/>
                                                <StarIcon className='w-10 h-10'/>
                                            </div>

                                            <button
                                                onClick={() => dispatch( addToCart({
                                                    id: item.id,
                                                    title: item.title,
                                                    description: item.description,
                                                    price: item.price,
                                                    category: item.category,
                                                    image: item.image,
                                                    quantity: 1,    
                                                }))}
                                                className='w-52 text-black font-titleFont font-medium text-xs md:text-sm bg-gradient-to-tr from-yellow-400 
                                                to-yellow-200 border rounded-md mt-3
                                                hover:from-yellow-300 hover:to-yellow-200 border-yellow-500 hover:border-yellow-700 
                                                active:bg-gradient-to-bl active:from-yellow-400 active:to-yellow-500 duration-200 py-1 md:py-1.5'
                                            >
                                                Add to Cart
                                            </button>
                                        </div>
                                    </div>
                                    
                                    <button
                                    onClick={() => setSelectedProduct(null)}
                                    className="absolute -top-3 -right-4 z-50 bg-gray-200 p-2 rounded-full text-black hover:bg-red-200 duration-200"
                                    >
                                        <Close/>
                                    </button>
                                </motion.div>
                            </motion.div>
                        )}

                    </div>
                ))
            }
        </div>
    )
}

export default Products;
