import React from 'react'
import Slider from "react-slick";
import { banner1, banner2, banner3, banner4, banner5, banner6 } from "../../assets/banner/index"

const Banner = () => {
    const settings = {
        dots: true,
        infinite: true,
        autoplay: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        zIndex: 5,
        appendDots: dots => (
            <div
              style={{
                backgroundColor: "transparent",
                borderRadius: "10px",
                padding: "10px",
                position: "relative",
                top: "-3.5em",
                color: "yellow",
              }}
            >
              <ul style={{ margin: "0px", color: 'yellow' }}> {dots} </ul>
            </div>
          ),  
          customPaging: i => (
            <div
              style={{
                width: "10px",
                height: "10px",
                backgroundColor: "#febd69",
                border: "1px white solid",
                borderRadius: "50%",
              }}
            >
                {/* {i + 1} */}
            </div>
          ), 
          responsive:[
            {
              breakpoint: 576,
              settings: {
                dots: true,
                appendDots: dots => (
                  <div
                    style={{
                      backgroundColor: "transparent",
                      borderRadius: "10px",
                      padding: "10px",
                      position: "relative",
                      top: "-3.8em",
                      color: "yellow",
                    }}
                  >  
                    <ul style={{ margin: "0px", color: 'yellow' }}> {dots} </ul>
                  </div>
                ),
                customPaging: i => (
                  <div
                    style={{
                      width: "5px",
                      height: "5px",
                      backgroundColor: "#febd69",
                      border: "1px black solid",
                      borderRadius: "50%",
                    }}
                  >
                      {/* {i + 1} */}
                  </div>
                )  
              }
            }
          ]
    };

  return (
    <div className='w-full'>
        <div className='w-full h-full relative'>
            <Slider {...settings} className='h-[25rem] md:h-[32rem]'>
                <div className='w-full h-[25rem] md:h-[32rem] relative'>
                    <img src={banner1} alt='banner1' className='object-cover w-full h-full'/>
                    <div className='absolute top-0 left-0 w-full h-full flex justify-center items-center flex-col bg-black bg-opacity-40'>
                        <h1 className='text-mart_yellow text-3xl barlow font-semibold'>HELLO</h1>
                    </div>
                </div>
                <div className='w-full h-[25rem] md:h-[32rem]'>
                    <img src={banner2} alt='banner2' className='object-cover w-full h-full'/>
                </div>
                <div className='w-full h-[25rem] md:h-[32rem]'>
                    <img src={banner3} alt='banner3' className='object-cover w-full h-full'/>
                </div>
                <div className='w-full h-[25rem] md:h-[32rem]'>
                    <img src={banner4} alt='banner4' className='object-cover w-full h-full'/>
                </div>
                <div className='w-full h-[25rem] md:h-[32rem]'>
                    <img src={banner5} alt='banner5' className='object-cover w-full h-full'/>
                </div>
                <div className='w-full h-[25rem] md:h-[32rem]'>
                    <img src={banner6} alt='banner6' className='object-cover w-full h-full'/>
                </div>
            </Slider>
        </div>
    </div>
  )
}

export default Banner