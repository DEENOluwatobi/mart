export const allItems=[
    { _id: "100", title: "All Departments" },
    { _id: "102", title: "Automotive" },
    { _id: "103", title: "Baby" },
    { _id: "104", title: "Beauty & Personal Care" },
    { _id: "105", title: "Books" },
    { _id: "106", title: "Boy's Fashion" },
    { _id: "107", title: "Computers" },
    { _id: "108", title: "Deals" },
    { _id: "109", title: "Digital Music" },
    { _id: "110", title: "Electronics" },
    { _id: "111", title: "Girls's Fashion" },
    { _id: "112", title: "Health & Household" },
    { _id: "113", title: "Home & Kitchen" },
    { _id: "114", title: "Industrial & Scientific" },
    { _id: "115", title: "Kindle Store" },
    { _id: "116", title: "Luggage" },
    { _id: "117", title: "Men's Fashion" },
    { _id: "118", title: "Movies & TV" },
    { _id: "119", title: "Music & Vinyl" },
    { _id: "120", title: "Pet Supplies" },
    { _id: "121", title: "Prime Video" },
    { _id: "122", title: "Software" },
    { _id: "123", title: "Sport & Outdoor" },
    { _id: "124", title: "Tools & Home Improvement" },
    { _id: "125", title: "Toys & Games" },
    { _id: "126", title: "Video Games" },
    { _id: "127", title: "Women's Fashion" },
];

export const middleList = [
    {
        _id: 2221,
        title: 'Get To Know Us',
        listItem: [
            {
                _id: "001",
                listData: [
                    "Careers",
                    "Blog",
                    "About Us",
                    "Investor Relations",
                    "Devices",
                    "Science",
                ],
            },
        ],
    },

    {
        _id: 2222,
        title: 'Make Money With Us',
        listItem: [
            {
                _id: "002",
                listData: [
                    "Sell Products on BigBoss",
                    "Sell Products on BigBoss Business",
                    "Sell Apps on BigBoss",
                    "Become An Affiliate",
                    "Advertise Your Products",
                    "Sell Products With Us",
                    "Host A Hub",
                    "See More To Make Money With Us",
                ],
            },
        ],
    },

    {
        _id: 2223,
        title: 'BigBoss Payment Products',
        listItem: [
            {
                _id: "003",
                listData: [
                    "BigBoss Business Card",
                    "Shop With Points",
                    "Reload Your Balance",
                    "BigBoss Currency Converter",
                ],
            },
        ],
    },

    {
        _id: 2224,
        title: 'Let Us Help You',
        listItem: [
            {
                _id: "004",
                listData: [
                    "COVID-19 Updates",
                    "Your Account",
                    "Your Orders",
                    "Shipping Rates & Policies",
                    "Return & Replacements",
                    "Manage Your Content & Devices",
                    "Assistant",
                    "FAQ & Help",
                ],
            },
        ],
    },
];

export const bottomList = [
    {
        _id: 1001,
        title: "BB Music",
        des: "Stream millions of songs",
    },
    {
        _id: 1002,
        title: "BB Advertising",
        des: "Find, attract & engage customers",
    },
    {
        _id: 1003,
        title: "BB Drive",
        des: "Cloud Storage from BB",
    },
    {
        _id: 1004,
        title: "6pm",
        des: "Score Deals on Fashion Brands",
    },
    {
        _id: 1005,
        title: "AceBooks",
        des: "Books, Art & Collectibles",
    },
    {
        _id: 1006,
        title: "ACX",
        des: "Audiobooks, Publishing Made Easy",
    },
    {
        _id: 1007,
        title: "Sell on BigBoss",
        des: "Sell a Selling Account",
    },
    {
        _id: 1008,
        title: "BB Business",
        des: "Everything For Your Business",
    },
    {
        _id: 1009,
        title: "BB Global",
        des: "Ship Orders Nationwide",
    },
    {
        _id: 1010,
        title: "Home Services",
        des: "Experienced Pros Happiness Guarantee",
    },
    {
        _id: 1011,
        title: "BB Ignite",
        des: "Sell your original Digital Educational Resources",
    },
    {
        _id: 1012,
        title: "BB Web Services",
        des: "Scalable Cloud Computing Services",
    },
    {
        _id: 1013,
        title: "Audible",
        des: "Listen to Books & Original Audio Performance",
    },
    {
        _id: 1014,
        title: "Book Depository",
        des: "Bookds With Free Delivery Nationwide",
    },
    {
        _id: 1015,
        title: "Box Office",
        des: "Find Movie Box Office Data",
    },
    {
        _id: 1016,
        title: "ComiXology",
        des: "Thousands of Digital Comics",
    },
    {
        _id: 1017,
        title: "DPReview",
        des: "Digital Photography",
    },
    {
        _id: 1018,
        title: "Fabric",
        des: "Sewing, Quilting & Knitting",
    },
    {
        _id: 1019,
        title: "Good Reads",
        des: "Book Reviews & Recommendations",
    },
    {
        _id: 1020,
        title: "IMDb",
        des: "Movies, TV & Celebrities",
    },
    {
        _id: 1021,
        title: "IMDbPro",
        des: "Get Info Entertainment Professionals Need",
    },
    {
        _id: 1022,
        title: "Kindle Direct Publishing",
        des: "Digital & Print Publishing Made Easy",
    },
    {
        _id: 1023,
        title: "Prime Video Direct",
        des: "Video Distribution Made Easy",
    },
    {
        _id: 1024,
        title: "Shopbop",
        des: "Designer Fashion Brands",
    },
    {
        _id: 1025,
        title: "Woot!",
        des: "Deals & Shenanigans",
    },
    {
        _id: 1026,
        title: "Zappos",
        des: "Shoes & Clothings",
    },
    {
        _id: 1027,
        title: "Ring",
        des: "Smart Home & Security Systems",
    },
    {
        _id: 1028,
        title: "BackYard",
        des: "First Grade Thrift Collections",
    },
    {
        _id: 1029,
        title: "Security",
        des: "Smart Security for Every Home",
    },
    {
        _id: 1030,
        title: "Neighbour's App",
        des: "Realtime Crime & Safety Alerts",
    },
    {
        _id: 1031,
        title: "Subscription Boxes",
        des: "Top subscription boxex - right to yout door",
    },
    {
        _id: 1032,
        title: "PillPack",
        des: "Pharmacy Simplified",
    },
];