import { initializeApp } from "firebase/app";
import { getAnalytics } from "firebase/analytics";

const firebaseConfig = {
  apiKey: "AIzaSyAEPfbZA1pFSt8MjpdPKz45prNZixdtj0w",
  authDomain: "bigboss-e6802.firebaseapp.com",
  projectId: "bigboss-e6802",
  storageBucket: "bigboss-e6802.appspot.com",
  messagingSenderId: "694780863961",
  appId: "1:694780863961:web:8080bec559c39c654f65c8",
  measurementId: "G-EVSV36KJ2B"
};

export const app = initializeApp(firebaseConfig);
export const analytics = getAnalytics(app);

export default firebaseConfig;