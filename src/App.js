import React from "react";
import {
  createBrowserRouter,
  Route,
  Outlet,
  createRoutesFromElements,
  RouterProvider,
  ScrollRestoration,
} from "react-router-dom";
import Header from "./components/header/Header";
import Footer from "./components/footer/Footer";
import Home from "./pages/Home";
import { productData } from "./api/api";
import Signin from "./pages/Signin";
import Cart from "./pages/Cart";
import { ThemeProvider } from "./context/ThemeContext";
import NotFound from "./pages/NotFound";
import SignUp from "./pages/SignUp";
import CheckOut from "./pages/CheckOut";
import Sell from "./sellers/Sell";
import SellersLogin from "./sellers/sellersLogin";
import SellersRegister from "./sellers/sellersRegister";
import ProductForm from "./sellers/NewProduct";
import SideNav from "./components/navbars/SideNav";

const Layout = () => {
  return(
    <div>
      <Header/>
      <div className="w-full flex relative h-[100vh]">
        <div className="w-[6%] sticky bg-mart_blue">
          <SideNav/>
        </div>
        <div className="w-[94%] h-[100vh] overflow-scroll">
          <ScrollRestoration/>
          <Outlet/>
          <Footer/>
        </div>
      </div>
    </div>
  )
}

function App() {
  const router = createBrowserRouter(createRoutesFromElements(
    <Route>
      <Route path="/" element={<Layout />}>
        <Route index element={<Home/>} loader={productData}></Route>
        <Route path="/cart" element={<Cart/>}></Route>
        <Route path="*" element={<NotFound/>} ></Route>
        <Route path="/sell" element={<Sell/>}></Route>
      </Route>
      <Route path="/signin" element={<Signin/>}></Route>
      <Route path="/signup" element={<SignUp/>}></Route>
      <Route path="/seller-sign-in" element={<SellersLogin/>}></Route>
      <Route path="/seller-register" element={<SellersRegister/>}></Route>
      <Route path="/new" element={<ProductForm/>}></Route>
      <Route path="/checkout" element={<CheckOut/>}></Route>
    </Route>
  ))
  
  return (
    <div>
      <ThemeProvider>
        <div className="font-bodyFont bg-gray-100">
          <RouterProvider router={router}></RouterProvider>
        </div>
      </ThemeProvider>
    </div>
  );
}

export default App;

