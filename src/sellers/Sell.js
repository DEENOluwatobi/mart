import React from 'react'
import { Link } from 'react-router-dom'
import { useTheme } from '../context/ThemeContext'

const Sell = () => {
  const {theme} = useTheme();
  const styles = {
    container: {
      borderRadius: '25px',
      background: theme === 'dark' ? '#131921' : '#e0e0e0',
      boxShadow: theme === 'dark' ? '10px 10px 50px #131921, -10px -10px 50px #232f3e' 
                                  : '10px 10px 50px #bebebe, -10px -10px 50px #e0e0e0',
    }
  };


  return (
    <div className='w-full'>
      <div className='sellpage w-full h-[30em] flex justify-center items-center'>
        <div className='bg-black bg-opacity-50 w-full h-full flex flex-col items-center justify-center gap-3'>
          <h1 className='text-yellow-300 text-[4.2em] font-bold'>Sell with BigBoss</h1>
          <p className='w-full text-center flex items-center justify-center text-zinc-200 gap-3'>
            <span>Elevate Your Brand</span>
            <span className='bg-gray-300 rounded-full h-2 w-2'></span>
            <span>Boost Your Sales</span>
            <span className='bg-gray-300 rounded-full h-2 w-2'></span>
            <span>Gain Recognition</span>
          </p>
          <p className='text-gray-200 w-[50%] text-center text-[1em]'>
            The acquisition channel that has experienced the most rapid 
            growth and is favored by more than half of our sellers across multiple 
            channels.
          </p>
          <div className='w-full flex justify-center items-center gap-3'>
            <Link to='/seller-register' className='mt-5'>
              <p className='px-14 py-2 border-[1.6px] border-gray-100 bg-yellow-300 hover:border-yellow-400 shadow-sm shadow-gray-500 duration-200 hover:shadow-yellow-300 rounded-md text-mart_blue'>
                Register
              </p>
            </Link>  
            <Link to='/seller-sign-in' className='mt-5'>
              <p className='px-14 py-2 border-[1.6px] border-gray-100 hover:border-yellow-400 hover:bg-black hover:bg-opacity-30 shadow-sm shadow-gray-500 duration-200 hover:shadow-yellow-300 rounded-md text-white'>
                Sign in
              </p>
            </Link>    

          </div>
        </div>
      </div>

      <div className={`${theme === 'dark' ? 'bg-mart_light' : 'bg-gray-100'}`}>
        <div className={`w-[75%] mx-auto grid grid-cols-2 px-4 py-6 gap-4`}>

            <div className='flex gap-4'>
              <div 
                style={styles.container}
                className={`${theme === 'dark' ? '' : ''} flex flex-col gap-3 px-4 py-6`}
              >
                <h1 className={`${theme === 'dark' ? 'text-mart_yellow' : 'text-mart_blue'} text-[1.5em] font-semibold font-titleFont`}>Why Sell With Us?</h1>
                <p className={`${theme === 'dark' ? 'text-gray-300' : 'text-gray-700'} text-[.8em]`}>
                  At BigBoss, we understand the importance of a thriving marketplace for sellers. 
                  Whether you're a seasoned pro or just starting, our platform provides the ideal 
                  environment for your business to flourish.
                </p>
              </div>

              <div 
                style={styles.container}
                className={`${theme === 'dark' ? '' : ''} flex flex-col gap-3 px-4 py-6`}
              >
                <h1 className={`${theme === 'dark' ? 'text-mart_yellow' : 'text-mart_blue'} text-[1.5em] font-semibold font-titleFont`}>Unparalleled Exposure</h1>
                <p className={`${theme === 'dark' ? 'text-gray-300' : 'text-gray-700'} text-[.8em]`}>
                  Join a vast community of sellers and showcase your products to a global audience. 
                  Benefit from our robust marketing strategies and reach customers beyond borders.
                </p>
              </div>
            </div>

            <div>
              <div 
                style={styles.container}
                className={`${theme === 'dark' ? '' : ''} flex flex-col gap-3 px-4 py-6`}
              >
                <h1 className={`${theme === 'dark' ? 'text-mart_yellow' : 'text-mart_blue'} text-[1.5em] font-semibold font-titleFont`}>Easy-to-Use Interface</h1>
                <p className={`${theme === 'dark' ? 'text-gray-300' : 'text-gray-700'} text-[.8em]`}>
                  Selling shouldn't be complicated. Our user-friendly interface allows you 
                  to effortlessly manage your inventory, track sales, and connect with customers. 
                  Spend more time doing what you love – growing your business.
                </p>
              </div>

              <div 
                style={styles.container}
                className={`${theme === 'dark' ? '' : ''} flex flex-col gap-3 px-4 py-6`}
              >
                <h1 className={`${theme === 'dark' ? 'text-mart_yellow' : 'text-mart_blue'} text-[1.5em] font-semibold font-titleFont`}>Secure Transactions</h1>
                <p className={`${theme === 'dark' ? 'text-gray-300' : 'text-gray-700'} text-[.8em]`}>
                  Rest easy knowing that every transaction is secure on our platform. Our 
                  advanced security measures protect both buyers and sellers, creating a safe 
                  and trustworthy marketplace.
                </p>
              </div>
            </div>

            <div 
              style={styles.container}
              className={`${theme === 'dark' ? '' : ''} flex flex-col gap-3 px-4 py-6 col-span-2`}
            >
              <h1 className={`${theme === 'dark' ? 'text-mart_yellow' : 'text-mart_blue'} text-[1.5em] font-semibold font-titleFont`}>Marketing Support</h1>
              <p className={`${theme === 'dark' ? 'text-gray-300' : 'text-gray-700'} text-[.8em]`}>
                The marketing support offered by BigBoss is designed to help sellers 
                effortlessly advertise their products and boost visibility. With clear images, quantities, 
                and prices in their catalog, sellers can take advantage of promotional tools provided by the platform. 
                The streamlined marketing process allows sellers to manage listings with ease, while targeted 
                campaigns ensure maximum impact on specific demographics or regions. Real-time analytics provide 
                valuable insights, empowering sellers to refine their strategies for optimal success. 
                Join BigBoss today to elevate your brand and increase sales with powerful marketing support.
              </p>
            </div>

        </div>
      </div>

      <div className={`${theme === 'dark' ? 'bg-mart_blue' : 'bg-mart_blue'} w-full z-10 p-4`}>
        <div className={`${theme === 'dark' ? '' : ''} w-[75%] mx-auto grid grid-cols-2 gap-4`}>
          <div className='px-4 py-6 text-white flex flex-col gap-3'>
            <h2 className='text-mart_yellow font-semibold text-[1.5em]'>How It Works</h2>
            <ul className='flex flex-col gap-2'>
              <li>
                <p className='text-[.8em]'>
                  <span className='text-mart_yellow'>1. Sign Up:  </span>
                  <span>Create your account in minutes. Provide some basic information, and you're ready to start selling.</span>
                </p>
              </li>

              <li>
                <p className='text-[.8em]'>
                  <span className='text-mart_yellow'>2. Upload Your Products:  </span>
                  <span>Showcase your products with high-quality images and detailed descriptions. The better your presentation, the more likely customers are to choose your products.</span>
                </p>
              </li>

              <li>
                <p className='text-[.8em]'>
                  <span className='text-mart_yellow'>3. Set Your Prices:  </span>
                  <span>You have control over your pricing. Determine the right balance between competitiveness and profitability to maximize your sales.</span>
                </p>
              </li>

              <li>
                <p className='text-[.8em]'>
                  <span className='text-mart_yellow'>4. Manage Orders:  </span>
                  <span>Stay organized with our order management system. Track orders, update shipping information, and communicate with buyers – all in one place.</span>
                </p>
              </li>

              <li>
                <p className='text-[.8em]'>
                  <span className='text-mart_yellow'>5. Get Paid:  </span>
                  <span>Enjoy hassle-free payments. Once a sale is made, funds are securely deposited into your account.</span>
                </p>
              </li>

            </ul>
            
          </div>
          <div className='how h-full w-full rounded-lg'></div>
        </div>
      </div>
    </div>
  )
}

export default Sell