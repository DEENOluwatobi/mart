import React, { useState } from 'react';
import { getAuth } from 'firebase/auth';
import { getFirestore, collection, addDoc } from 'firebase/firestore';
import { app } from '../firebase.config';
import SellerNav from './sellersComponents/SellerNav';
import { useSelector } from 'react-redux';
import Footer from '../components/footer/Footer';
import SellerNotLoggedIn from './sellersComponents/SellerNotLoggedIn';
import { BrandName, Category, DeleteCan, Details, ImagePlus, Location, Naira, Plus, Size, Tag, Units } from '../icons';
import { Delete, Image } from '@mui/icons-material';

const ProductForm = () => {
  const auth = getAuth(app); 
  const db = getFirestore(app); 
  const userInfo = useSelector((state) => state.BBReducer.userInfo);

  const [selectedImage, setSelectedImage] = useState(null);
  const handleImageChange = (event) => {
    const file = event.target.files[0];

    if (file) {
      const reader = new FileReader();
      reader.onloadend = () => {
        setSelectedImage(reader.result);
      };
      reader.readAsDataURL(file);
    }
  };

  const handleRemoveImage = () => {
    setSelectedImage(null);
  };

  const [productData, setProductData] = useState({
    title: '',
    price: '',
    category: '',
    details: '',
    image: '',
    location: '',
    brandName: '',
    size: '',
    units: '',
  });

  const handleInputChange = (e) => {
    const { name, value } = e.target;
    setProductData((prevData) => ({ ...prevData, [name]: value }));
  };

  const handleUpload = async () => {
    const user = auth.currentUser;

    if (!user) {
      // Redirect to login or show an error
      console.error('User not authenticated');
      return;
    }

    try {
      const productRef = await addDoc(collection(db, 'products'), {
        sellerId: user.uid,
        ...productData,
      });
      

      console.log('Product uploaded with ID: ', productRef.id);

      // Clear form fields after successful upload
      setProductData({
        title: '',
        price: '',
        category: '',
        details: '',
        image: '',
        location: '',
        brandName: '',
        size: '',
        units: '',
      });
    } catch (error) {
      console.error('Error uploading product:', error);
    }
  };

  return (
    <div className='relative'>
      <div className='sticky'>
        <SellerNav/>
      </div>

      <div className='w-full sellBG'>
        {
          userInfo ? (
            <div className='w-full'>
                <div className='w-full mx-auto flex items-center justify-center gap-2'>
                  <span className='text-mart_yellow font-semibold font-titleFont text-[2em]'>Welcome</span>
                  <span className='text-zinc-100 font-semibold font-titleFont text-[3em]'> {userInfo.userName}</span>
                </div>

                <div className='w-full grid grid-cols-2 px-4 py-4 gap-3'>
                  <div className='w-full h-full bg-mart_blue rounded-md px-6 py-4 backdrop-blur-md bg-opacity-40 flex flex-col text-white justify-start overflow-hidden'>
                    <span className='text-mart_yellow font-semibold font-titleFont text-[1.2em] mb-1'>Add New Product</span>

                    <form className='flex flex-col gap-2'>

                      <div className='grid grid-cols-2 gap-2'>
                          <div className='w-full flex items-center gap-3 border-b-[1px] border-zinc-400 px-2 py-1'>
                            <label className='text-zinc-100 flex items-center justify-center gap-1'>
                              <Tag className='[&>path]:fill-[#febd69]' size='md'/>
                              TITLE:
                            </label>
                            <input
                              type="text"
                              name="title"
                              value={productData.title}
                              onChange={handleInputChange}
                              className='bg-transparent outline-none border-none rounded-sm flex-grow'
                            />
                          </div>

                          <div className='w-full flex items-center gap-3 border-b-[1px] border-zinc-400 px-2 py-1'>
                            <label className='text-zinc-100 flex items-center justify-center gap-1'>
                              <Naira className='[&>path]:fill-[#febd69]' size='sm'/>
                              PRICE:
                            </label>
                            <input
                              type="number"
                              name="price"
                              value={productData.price}
                              onChange={handleInputChange}
                              className='bg-transparent outline-none border-none rounded-sm flex-grow appearance-none no-number-arrows'
                            />
                          </div>  
                      </div>

                      <div className='w-full flex items-center gap-3 border-b-[1px] border-zinc-400 px-2 py-1'>
                        <label className='text-zinc-100 flex items-center justify-center gap-1'>
                          <Category className='[&>path]:fill-[#febd69]' size='sm'/>
                          CATEGORY:
                        </label>
                        <input
                          type="text"
                          name="category"
                          value={productData.category}
                          onChange={handleInputChange}
                          className='bg-transparent outline-none border-none rounded-sm flex-grow'
                        />
                      </div>

                      <div className='grid grid-cols-3 gap-2'>
                        
                        <div className='w-full col-span-1 flex flex-col items-start border-[1px] border-zinc-400 px-2 py-1'>
                          <label className='text-zinc-100 flex items-center justify-center gap-1'>
                            <Image className='[&>path]:fill-[#febd69]' size='sm'/>
                            IMAGE:
                          </label>

                          <label className='relative w-full cursor-pointer rounded-md overflow-hidden '>
                            <span className={`${selectedImage ? 'hidden' : 'flex'} w-full h-full text-white py-1 px-3 mt-2 flex justify-center items-center`}>
                              {selectedImage ? '' : <ImagePlus className='[&>path]:stroke-[#fff]' size='lg'/>}
                            </span>
                            <input
                              type="file"
                              accept="image/*"
                              name="image"
                              onChange={handleImageChange}
                              className='opacity-0 absolute top-0 left-0 cursor-pointer h-full w-full'
                            />
                          </label>
                          {selectedImage && (
                            <div className='relative flex items-center justify-center overflow-hidden h-full w-full '>
                              <img src={selectedImage} alt='Selected' className='object-cover h-24 w-24' />
                              <button
                                onClick={handleRemoveImage}
                                className='absolute top-0 right-0 px-2 py-1 text-xs cursor-pointer border-[2px] flex items-center justify-center rounded-full border-red-500 w-8 h-8 bg-white'
                              >
                               <DeleteCan className='[&>path]:stroke-[#ef1515]'/>
                              </button>
                      
                            </div>
                          )}
                        </div>

                        <div className='w-full col-span-2 flex items-start gap-3 border-[1px] border-zinc-400 px-2 py-1'>
                          <label className='text-zinc-100 flex items-center justify-center gap-1'>
                            <Details className='[&>path]:fill-[#febd69]' size='sm'/>
                            DETAILS:
                          </label>
                          <textarea
                            name="details"
                            value={productData.details}
                            onChange={handleInputChange}
                            className='bg-transparent outline-none border-none rounded-sm flex-grow h-24 resize-none'
                          ></textarea>
                        </div>
                      </div>

                      <div className='grid grid-cols-2 gap-2'>
                            <div className='w-full flex items-center gap-3 border-b-[1px] border-zinc-400 px-2 py-1'>
                              <label className='text-zinc-100 flex items-center justify-center gap-1'>
                                <Location className='[&>path]:fill-[#febd69]' size='sm'/>
                                LOCATION:
                              </label>
                              <input
                                type="text"
                                name="location"
                                value={productData.location}
                                onChange={handleInputChange}
                                className='bg-transparent outline-none border-none rounded-sm flex-grow'
                              />
                            </div>

                            <div className='w-full flex items-center gap-3 border-b-[1px] border-zinc-400 px-2 py-1'>
                              <label className='text-zinc-100 flex items-center justify-center gap-1'>
                                <BrandName className='[&>path]:fill-[#febd69] -ml-1' />
                                BRAND:
                              </label>
                              <input
                                type="text"
                                name="brandName"
                                value={productData.brandName}
                                onChange={handleInputChange}
                                className='bg-transparent outline-none border-none rounded-sm flex-grow'
                              />
                            </div>
                      </div>

                      <div className='grid grid-cols-2 gap-2'>

                            <div className='w-full flex items-center gap-3 border-b-[1px] border-zinc-400 px-2 py-1'>
                              <label className='text-zinc-100 flex items-center justify-center gap-1'>
                                <Size className='[&>path]:fill-[#febd69]' size='sm'/>
                                SIZE:
                              </label>
                              <input
                                type="text"
                                name="size"
                                value={productData.size}
                                onChange={handleInputChange}
                                className='bg-transparent outline-none border-none rounded-sm flex-grow'
                              />
                            </div>

                            <div className='w-full flex items-center gap-3 border-b-[1px] border-zinc-400 px-2 py-1'>
                              <label className='text-zinc-100 flex items-center justify-center gap-1'>
                                <Units className='[&>path]:fill-[#febd69] mb-1' size='sm'/>
                                UNITS:
                              </label>
                              <input
                                type="number"
                                name="units"
                                value={productData.units}
                                onChange={handleInputChange}
                                className='bg-transparent outline-none border-none rounded-sm flex-grow appearance-none'
                              />
                            </div>
                      </div>

                      <button type="button" onClick={handleUpload} className='px-14 py-2 border-[1.6px] border-gray-100 bg-yellow-300 hover:border-yellow-400 shadow-sm shadow-gray-500 duration-200 hover:shadow-yellow-300 rounded-md text-mart_blue'>
                        Upload Product
                      </button>
                    </form>
                  </div>

                  <div className='w-full h-full bg-mart_blue rounded-md p-4 mx-auto backdrop-blur-md bg-opacity-40 flex flex-col text-white justify-start items-center overflow-hidden'>
                    <h1>Products</h1>
                    <div className=''>

                    </div>

                  </div>
                </div>
            </div>
          ) : (
            <div className='w-full'>
              <SellerNotLoggedIn/> 
            </div>
          ) 
        }
      </div>
      
      <div>
        <Footer/>
      </div>

    </div>
  );
};

export default ProductForm;
