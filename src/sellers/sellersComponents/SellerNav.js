import React, {useState} from 'react'
import ArrowDropDownOutlinedIcon from '@mui/icons-material/ArrowDropDownOutlined';
import { Logout, Search } from '@mui/icons-material';
import { getAuth, signOut } from "firebase/auth";
import { allItems } from '../../constants';
import { Link } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { userSignOut } from '../../redux/BBSlice';
import ThemeBtn from "../../components/header/ThemeBtn"

const SellerNav = () => {
    const auth = getAuth();
    const dispatch = useDispatch();
    const  [showAll, setShowAll] = useState(false);
    const products = useSelector((state) => state.BBReducer.products);
    const userInfo = useSelector((state) => state.BBReducer.userInfo);
    // console.log(products)

    const handleLogout = () => {
        signOut(auth).then(() => {
            console.log("Sign-out successful");
            dispatch(userSignOut())
          }).catch((error) => {
            console.log("An error happened.")
          });
    }

  return (
    <div className='w-full sticky top-0 border-b-[1px] border-mart_blue z-50 shadow-sm shadow-gray-600'>
        <div className='w-full bg-mart_blue px-4 py-3 flex items-center text-white gap-5'>
            {/* =============== Image starts here ===============*/}
                <Link to="/new">
                    <div className='text-[#b4b5b7] font-semibold text-xl font-titleFont cursor-pointer'>
                        Big<span className='font-normal text-mart_yellow'>Boss</span>
                    </div>
                </Link>
            {/* =============== Image ends here ===============*/}

            {/* =============== Deliver starts here ===============*/}
                {/* <div className='items-center hidden mdl:inline-flex'>
                    <LocationOnOutlinedIcon className='text-white'/>
                    <div className='flex flex-col text-[0.6rem] justify-center font-bodyFont cursor-pointer font-light text-lightText'>
                        <span>Deliver to</span>
                        <span className='text-[0.7rem] font-semibold -mt-1'>Oluwatobi</span>
                    </div>
                </div> */}
            {/* =============== Deliver ends here ===============*/}

            {/* =============== Search starts here ===============*/}
                <div className='h-10 rounded-md hidden md:flex flex-grow relative '>
                    <span 
                    onClick={()=>setShowAll(!showAll)}
                    className='w-14 h-full bg-gray-200 hover:bg-gray-300 border-2 cursor-pointer duration-300 test-sm text-mart_blue font-titleFont flex items-center justify-center rounded-tl-md rounded-bl-md'
                    >
                        <span className='text-sm'>All</span>
                        <span>
                        {
                            showAll && (
                                <div>
                                    <ul className='absolute w-56 h-80 top-10 left-0 overflow-y-scroll overflow-x-hidden bg-white border-[1px] border-mart_blue text-black p-2 flex-col gap-1 z-50 rounded-md'>  
                                        {
                                            allItems.map((item)=>(
                                                <li 
                                                className='text-sm tracking-wide font-titleFont hover:bg-gray-200 px-1 rounded-md cursor-pointer duration-200'
                                                key={item._id}
                                                >
                                                    {item.title}
                                                </li>
                                            ))
                                        }

                                    </ul>
                                </div>
                            )
                        }
                        </span>
                        <ArrowDropDownOutlinedIcon/>
                    </span>
                    <input 
                        type="text" 
                        placeholder='Search products...' 
                        className='px-2 h-full text-sm text-mart_blue flex-grow outline-none border-none'
                    />
                    <span className='w-12 h-full flex items-center justify-center bg-mart_yellow hover:bg-[#f3a847] duration-300 text-mart_blue cursor-pointer rounded-tr-md rounded-br-md'>
                        <Search/>
                    </span>
                </div>    
            {/* =============== Search ends here ===============*/}

            {/* =============== Signin starts here ===============*/}
                <div className=''>    
                        <div className='flex flex-col items-start justify-center'>
                            {
                                userInfo ? (
                                    <p>
                                        {" "}
                                        <p className='text-[.8em] mdl:text-[.7em] text-white mdl:text-lightText'>
                                        Hi, {userInfo.userName}
                                        </p>
                                    </p>
                                ) : (
                                    <Link to="/seller-sign-in" className='flex flex-col items-start justify-center'>
                                        <p className='text-[.8em] mdl:text-[.7em] text-white mdl:text-lightText cursor-pointer hover:text-mart_yellow'>
                                            Hello, sign in
                                        </p>   
                                    </Link>     
                                )
                            }
                            <p className='text-[.7em] font-semibold hidden mdl:inline-flex justify-center items-center -mt-1'>
                                Accounts & Lists
                                <span><ArrowDropDownOutlinedIcon/></span>
                            </p>
                        </div>                  
                </div>        

            {/* =============== Signin ends here ===============*/}

            {/* =============== Orders starts here ===============*/}
                <div className='hidden lgl:flex flex-col items-start justify-center cursor-pointer'>
                    <p className='text-[.7em] text-lightText'>Returns</p>
                    <p className='text-[.7em] font-semibold flex justify-center items-center -mt-1'>& Orders</p>
                </div>

            {/* =============== Orders ends here ===============*/}

            {/* =============== Cart starts here ===============*/}
                {/* <Link to="/cart">
                    <div className='flex items-start justify-center cursor-pointer relative'>
                        <ShoppingCart/>
                        <p className='text-[.7em] font-semibold mt-[0.72rem] text-whiteText'>
                            Cart
                            <span className={`${products.length === 0 ? 'hidden' : 'flex' } absolute text-[.7em -top-1 left-4 font-semibold w-4 h-4 bg-[#f3a847] text-mart_blue rounded-full justify-center items-center`}>
                                { products.length }
                            </span>
                        </p>
                    </div>
                </Link> */}
            {/* =============== Cart ends here ===============*/}

            {/* =============== Theme control button starts here ===============*/}
            <div>
                <ThemeBtn/>
            </div>
            {/* =============== Theme control button ends here ===============*/}

            {/* ===================== Logout starts here================== */}
            {
                userInfo ? (
                    <p 
                    onClick={handleLogout}
                    className='text-[.7em] font-semibold hidden mdl:inline-flex justify-center items-center cursor-pointer'>
                        <span><Logout className='text-mart_yellow'/></span>
                    </p>
                ) : (
                    <div></div>
                )
            }
            {/* ===================== Logout ends here================== */}

        </div>
    </div>
    
  )
}

export default SellerNav