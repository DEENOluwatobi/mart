import React from 'react'
import { motion } from "framer-motion"
import caution from '../../assets/Images/caution-sign.png'
import { Link } from 'react-router-dom'

const SellerNotLoggedIn = () => {
   
    
  return (
    <div className='w-full sellerPNF h-[600px] px-6 py-10'>
        <div className='w-[50%] h-full bg-mart_blue rounded-md p-4 mx-auto backdrop-blur-md bg-opacity-40 flex flex-col text-white justify-start items-center overflow-hidden'> 
            <div className='w-full flex justify-center items-center'>
                <motion.img
                    src={caution}
                    alt='caution'
                    className='w-52 h-52'
                    animate={{ y: [-10, 10, -10] }}
                    transition={{ y: {
                        duration: 3,
                        ease: 'linear',
                        repeat: Infinity,
                    }}}
                />
            </div>
            <span className='text-titleFont text-mart_yellow font-semibold text-[2em] mb-2'>Uh-oh,</span>
            <span className='w-[80%] text-center text-gray-100 text-[.9em]'>
                It seems like the seller portal is locked! Time to put on your virtual ID 
                badge—hit that 'Sign In' button and let the selling extravaganza begin!
            </span>
            <Link to='/seller-sign-in'>
                <p className='px-14 py-2 border-[1.6px] border-gray-100 hover:border-yellow-400 hover:bg-black hover:bg-opacity-30 shadow-sm shadow-gray-500 duration-200 hover:shadow-yellow-300 rounded-md text-white mt-5'>
                    Sign in
                </p>
            </Link>
        </div>

    </div>
  )
}

export default SellerNotLoggedIn