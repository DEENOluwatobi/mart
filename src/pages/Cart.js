import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { useTheme } from '../context/ThemeContext';
import { CheckCircle } from '@mui/icons-material';
import { deleteItem, resetCart, incrementQuantity, decrementQuantity } from '../redux/BBSlice';
import EmptyCart from '../assets/Images/emptycart.png';
import { motion } from 'framer-motion';
import { Link } from 'react-router-dom';

const Cart = () => {
    const { theme } = useTheme();
    const dispatch = useDispatch();
    const products = useSelector((state) => state.BBReducer.products);
    const [ totalPrice, setTotalPrice ] = useState("");
    useEffect(() => {
        let Total = 0;
        products.map((item) => {
            Total += item.price * item.quantity;
            return setTotalPrice(Total.toFixed(2));
        })
    });

  return (
    <div className={`${theme === "dark" ? "bg-gray-800" : "bg-gray-100"} w-full p-4`}>
        {
            products.length > 0 ? (
                <div className='container mx-auto h-auto grid grid-cols-5 gap-6'>
                    <div className={`${theme === "dark" ? "bg-mart_blue" : "bg-white"} w-full px-4 col-span-4 rounded-sm`}>
                        <div className='font-titleFont flex items-center justify-between border-b-[1px] border--gray-400 py-2'>
                            <h2 className={`${theme === 'dark' ? 'text-white' : 'text-black'} text-xl font-medium`}>Shopping cart</h2>
                            <h4 className={`${theme === 'dark' ? 'text-white' : 'text-black'} text-lg font-normal`}>Sub total</h4>
                        </div>

                        {/* ============ CART PRODUCTS/ITEMS STARTS HERE ============ */}

                        <div className='w-full'>
                            {
                                products.map((item) =>(
                                    <div key={item.id} className={`${theme === 'dark' ? 'border-b-mart_light' : "border-b-gray-300 "} w-full border-b-[1px] p-4 grid grid-cols-7 items-center gap-6`}>
                                        <div className='w-full col-span-1'>
                                            <img 
                                                src={item.image} 
                                                alt="product-img" 
                                                className={`${theme === 'dark' ? 'border border-mart_yellow rounded-md bg-white' : 'bg-white'} w-20 h-32 object-contain`}
                                            />
                                        </div>

                                        <div className='col-span-5'>
                                            <div>
                                                <h2 className={`${theme === 'dark' ? 'text-yellow-600' : 'text-black'} font-semibold text-base`}>{item.title}</h2>
                                                <p className={`${theme === 'dark' ? 'text-gray-400' : 'text-black'} pr-10 text-[.8em]`}>{item.description}</p>
                                                <p className={`${theme === 'dark' ? 'text-white' : 'text-black'} text-sm mt-1`}>Unit price {" "}
                                                    <span className={`${theme === 'dark' ? 'text-white' : 'text-black'} font-semibold`}>${item.price}</span>
                                                </p>

                                                <div className={`${theme === 'dark' ? 'bg-gray-600' : 'bg-[#f0f2f2]'} flex justify-center items-center gap-2 w-28 py-1 text-center drop-shadow-lg rounded-md`}>
                                                    <p className={`${theme === 'dark' ? 'text-gray-300' : 'text-black'} text-xs`}>Qty</p>
                                                    
                                                    <p 
                                                    onClick={()=>dispatch(decrementQuantity(item.id))}
                                                    className={`${theme === 'dark' ? 'bg-gray-700 hover:bg-gray-800 text-gray-300' : 'bg-gray-200 hover:bg-gray-400'} cursor-pointer  rounded-md duration-300 px-2`}
                                                    >
                                                        -
                                                    </p>

                                                    <p className={`${theme === 'dark' ? 'text-gray-300' : 'text-black'} text-xs`}>{item.quantity}</p>

                                                    <p 
                                                    onClick={()=>dispatch(incrementQuantity(item.id))}
                                                    className={`${theme === 'dark' ? 'bg-gray-700 hover:bg-gray-800 text-gray-300' : 'bg-gray-200 hover:bg-gray-400'} cursor-pointer  rounded-md duration-300 px-2`}
                                                    >
                                                        +
                                                    </p>
                                                </div>

                                            </div>

                                            <button 
                                            className='bg-red-500 text-sm w-32 py-1 rounded-lg text-white mt-2 hover:bg-red-600 active:bg-red-800 duration-300'
                                            onClick={()=>dispatch(deleteItem(item.id))}
                                            >
                                                Remove item
                                            </button>
                                        </div>

                                        <div className='col-span-1 ml-auto'>
                                            <p className={`${theme === 'dark' ? 'text-white' : 'text-black'} text-base font-titleFont font-semibold`}>
                                                $ {(item.price * item.quantity).toFixed(2)}
                                            </p>
                                        </div>
                                    </div>
                                ))
                            }
                        </div>
                        <div onClick={()=>dispatch(resetCart())} className='w-full py-3 mx-auto flex justify-center items-center'>
                            <button className='px-10 py-2 bg-red-500 hover:bg-red-600 active:bg-red-500 text-white rounded-lg font-titleFont text-[.9em] tracking-wide'>
                                Clear Cart
                            </button>
                        </div>
                    </div>

                    <div className={`${theme === 'dark' ? 'bg-mart_blue text-gray-300' : 'bg-white'} w-full h-52 rounded-sm col-span-1 flex flex-col items-start p-4`}>
                        <div>
                            <p className={`${theme === 'dark' ? 'text-gray-300' : 'text-black'} flex gap-2 items-start text-[.8em]`}>
                                <span><CheckCircle className='text-green-500 bg-white rounded-full'/></span>
                                <span>
                                    Your order qualifies for FREE shipping. Choose this option at checkout.
                                    See details...
                                </span>
                            </p>
                        </div>
                        <div>
                            <p className='font-semibold text-sm px-8 py-1 flex items-center justify-between gap-1 mt-1'>
                                <span>Total:</span>
                                <span className='text-base font-bold'>
                                    ${totalPrice}
                                </span>
                            </p>
                        </div>

                        <Link className='w-full' to='/checkout'>
                            <div className='w-full'>
                                <button className='w-full font-titleFont font-medium text-sm bg-gradient-to-tr from-yellow-400 to-yellow-200
                                border hover:from-yellow-300 hover:to-yellow-500 border-yellow-500 hover:border-yellow-700 active:bg-gradient-to-bl 
                                active:from-yellow-400 active:to-yellow-500 duration-200 py-1.5 rounded-md mt-2 text-black'
                                >
                                    Proceed to Pay
                                </button>
                            </div>
                        </Link>
                    </div>
                </div>

            ) :  (

                <motion.div 
                initial={{y: 70, opacity: 0}}
                animate={{y: 0, opacity: 1}}
                transition={{delay: 0.1, duration: 0.5}}
                className={`flex justify-center items-center gap-4 py-5`}
                >
                    <div>
                        <img 
                            src={EmptyCart} 
                            alt='empty-cart-img'
                            className='w-80 rounded-lg p-4 mx-auto'
                        />
                    </div>

                    <div className={`${theme === 'dark' ? 'bg-mart_blue text-gray-300' : 'bg-white'} w-96 p-4 flex flex-col items-center rounded-md shadow-lg`}>
                        <h1 className={`font-titleFont text-lg font-bold`}>Your Cart is empty</h1>
                        <p className={`text-sm text-center`}>
                            {" "}
                            Your shopping cart lives to serve. Give it purpose - fill it with books, electronics, fashion items, jewelries etc. and make it happy
                        </p>
                        
                        <Link to='/'>
                            <button
                            className={`mt-6 bg-yellow-400 rounded-md cursor-pointer hover:bg-yellow-500 active:bg-yellow-700 text-black px-8 py-2 font-titleFont font-semibold`}
                            >
                                Continue shopping
                            </button>
                        </Link>
                    </div>
                </motion.div>
            )
        }
    </div>
  )
}

export default Cart