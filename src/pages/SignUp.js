import React, { useEffect, useState } from 'react'
import { ArrowRight } from '@mui/icons-material'
import { getAuth, createUserWithEmailAndPassword, updateProfile } from "firebase/auth";
import { Link, useNavigate } from 'react-router-dom'
import { ColorRing } from  'react-loader-spinner'
import { motion } from 'framer-motion'

const SignUp = () => {
  const auth = getAuth();
  const navigate = useNavigate();
  const [ currentYear, setCurrentYear ] = useState(new Date().getFullYear());

  const [ firstName, setFirstName ] = useState("");
  const [ lastName, setLastName] = useState("");
  const [ email, setEmail ] = useState("");
  const [ password, setPassword ] = useState("");
  const [ cPassword, setCPassword ] = useState("");

  const [ errFirstName, setErrFirstName ] = useState("");
  const [ errLastName, setErrLastName ] = useState("");
  const [ errEmail, setErrEmail ] = useState("");
  const [ errPassword, setErrPassword ] = useState("");
  const [ errCPassword, setErrCPassword ] = useState("");
  const [ errFirebase, setErrFirebase ] = useState("");

  const [ loading, setLoading ] = useState(false);
  const [ successMsg, setSuccessMsg ] = useState("");


  useEffect(() => {
    const intervalId = setInterval(() => {
      setCurrentYear(new Date().getFullYear());
    }, 1000);
    return () => clearInterval(intervalId);
  }, []);

  const handleFirstName = (e) => {
    setFirstName(e.target.value);
    setErrFirstName("");
  };
  const handleLastName = (e) => {
    setLastName(e.target.value);
    setErrLastName("");
  };
  const handleEmail = (e) => {
    setEmail(e.target.value);
    setErrEmail("");
    setErrFirebase("");
  };
  const handlePassword = (e) => {
    setPassword(e.target.value);
    setErrPassword("")
  };
  const handleCPassword = (e) => {
    setCPassword(e.target.value);
    setErrCPassword("");
  };

  // EMAIL VALIDATION-------------------------
  const emailValidation = (email) => {
    return String(email)
      .toLowerCase()
      .match(/^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/);
  }
  

  // SUBMIT FUNCTION----------------------------
  const handleRegistration = (e) => {
    e.preventDefault();
    if(!firstName){
      setErrFirstName("Enter First Name")
    };
    if(!lastName){
      setErrLastName("Enter Last Name")
    };
    if(!email){
      setErrEmail("Enter email")
    }else if(!emailValidation(email)){
      setErrEmail("Enter a valid email");
    }
    if(!password){
      setErrPassword("Enter password")
    }else if(password.length < 6){
      setErrPassword("Password must be at least 6 characters")
    };
    if(!cPassword){
      setErrCPassword("Confirm password")
    }else if(cPassword !== password){
      setErrCPassword("Password not matched")
    };

    if(firstName && lastName && email && emailValidation(email) && password && password.length >= 6 && cPassword && cPassword === password){
      setLoading(true)
      createUserWithEmailAndPassword(auth, email, password)
      .then((userCredential) => { 
        updateProfile(auth.currentUser, {
          displayName: firstName,
          photoURL: "https://www.freepik.com/free-psd/3d-illustration-human-avatar-profile_58509050.htm#query=png%20avatars&position=19&from_view=keyword&track=ais&uuid=2cd465f1-6d2b-4b5d-bd7f-356ae4b6d548"
        })
        // Signed up 
        const user = userCredential.user;
        // console.log(user)
        setLoading(false)
        setSuccessMsg("Account created successfully")
        setTimeout(()=>{
          navigate("/signin")
        }, 3000)
      })
      
      .catch((error) => {
        const errorCode = error.code;
        const errorMessage = error.message;
        if(errorCode.includes("auth/email-already-in-use")){
          setErrFirebase("Email has already been registered. Try another")
        }        
      });

      // setFirstName("");
      // setLastName("");
      // setEmail("");
      // setPassword("");
      // setCPassword("");
    }
  };

  return (
    <div className='w-full '>
        <div className='w-full p-10'>
          <div className='flex justify-center items-center text-[#5c5c5c] font-semibold text-2xl font-titleFont cursor-pointer'>
            Big<span className='font-normal text-yellow-500'>Boss</span>
          </div>

          <form className='w-[350px] mx-auto p-2'>
            <div className='flex flex-col w-full border border-zinc-200 p-6 '>
              <h2 className='font-titleFont text-xl font-medium mb-4'>Register</h2>
              <div className='flex flex-col gap-3'>
                <div className='w-full grid grid-cols-2 gap-2'>

                  <div className='flex flex-col gap-1'>
                    <p className='text-xs font-medium'>First name</p>
                    <input 
                      type='text' 
                      value={firstName}
                      className='w-full py-1 border border-zinc-400 px-2 text-sm rounded-sm outline-none focus-within:border-[#e77700] focus-within:shadow-martInput duration-100'
                      onChange={handleFirstName}
                    />
                    {
                      errFirstName && 
                        <p className='text-red-600 text-[.65em] font-semibold tracking-wide flex items-center gap-1 -mt-1.5'>
                          <span className='italic font-titleFont font-extrabold'>!</span>
                          {errFirstName}
                        </p>
                    }
                  </div>

                  <div className='flex flex-col gap-1'>
                    <p className='text-xs font-medium'>Last name</p>
                    <input 
                      type='text' 
                      value={lastName}
                      className='w-full py-1 border border-zinc-400 px-2 text-sm rounded-sm outline-none focus-within:border-[#e77700] focus-within:shadow-martInput duration-100'
                      onChange={handleLastName}  
                    />
                    {
                      errLastName &&
                        <p className='text-red-600 text-[.65em] font-semibold tracking-wide flex items-center gap-1 -mt-1.5'>
                          <span className='italic font-titleFont font-extrabold'>!</span>
                          {errLastName}
                        </p>
                    } 
                  </div>
                </div>

                <div className='flex flex-col gap-1'>
                  <p className='text-xs font-medium'>Email</p>
                  <input 
                    type='email' 
                    value={email}
                    className='w-full lowercase py-1 border border-zinc-400 px-2 text-sm rounded-sm outline-none focus-within:border-[#e77700] focus-within:shadow-martInput duration-100'
                    onChange={handleEmail}
                  />
                  {
                    errEmail &&
                      <p className='text-red-600 text-[.65em] font-semibold tracking-wide flex items-center gap-1 -mt-1.5'>
                        <span className='italic font-titleFont font-extrabold'>!</span>
                        {errEmail}
                      </p>
                  } 
                  {
                    errFirebase &&
                      <p className='text-red-600 text-[.65em] font-semibold tracking-wide flex items-center gap-1 -mt-1.5'>
                        <span className='italic font-titleFont font-extrabold'>!</span>
                        {errFirebase}
                      </p>
                  } 
                </div>

                <div className='flex flex-col gap-1'>
                  <p className='text-xs font-medium'>Password</p>
                  <input 
                    type='password' 
                    value={password}
                    onChange={handlePassword}
                    className='w-full py-1 border border-zinc-400 px-2 text-sm rounded-sm outline-none focus-within:border-[#e77700] focus-within:shadow-martInput duration-100'
                  />
                  {
                    errPassword &&
                      <p className='text-red-600 text-[.65em] font-semibold tracking-wide flex items-center gap-1 -mt-1.5'>
                        <span className='italic font-titleFont font-extrabold'>!</span>
                        {errPassword}
                      </p>
                  }
                  <p className='text-[.65em] text-gray-600'>Password must be at least 6 characters</p>
                </div>

                <div className='flex flex-col gap-1'>
                  <p className='text-xs font-medium'>Confirm password</p>
                  <input 
                    type='password' 
                    value={cPassword}
                    onChange={handleCPassword}
                    className='w-full py-1 border border-zinc-400 px-2 text-sm rounded-sm outline-none focus-within:border-[#e77700] focus-within:shadow-martInput duration-100'
                  />
                  {
                    errCPassword &&
                      <p className='text-red-600 text-[.65em] font-semibold tracking-wide flex items-center gap-1 -mt-1.5'>
                        <span className='italic font-titleFont font-extrabold'>!</span>
                        {errCPassword}
                      </p>
                  }
                </div>

                <button 
                onClick={handleRegistration} 
                className='w-full py-1.5 text-sm font-normal rounded-sm bg-gradient-to-t from-[#f7dfa5] to-[#f0c14b] hover:bg-gradient-to-b border border-zinc-400 activer:border-yellow-800 active:shadow-martInput'
                >
                  Create account
                </button>
                {
                  loading && (
                    <div className='flex justify-center items-center'>
                      <ColorRing
                        visible={true}
                        height="50"
                        width="50"
                        ariaLabel="blocks-loading"
                        wrapperStyle={{}}
                        wrapperClass="blocks-wrapper"
                        colors={['#b8c480', '#B2A3B5', '#F4442E', '#51E5FF', '#429EA6']}
                      />
                    </div>
                  )
                }
                {
                  successMsg && (
                    <div className='flex justify-center items-center w-full h-10 bg-green-200 border border-green-400 rounded-md'>
                      <motion.p 
                      initial={{y: 10, opacity: 0}}
                      animate={{y: 0, opacity: 1}}
                      transition={{duration: 0.5}}
                      className='text-green-500 font-titleFont text-center text-[0.65em]'>
                        {successMsg}
                      </motion.p>
                    </div>
                  )
                }
              </div>

              <p className='text-[.7em] text-black leading-4 mt-4'>
                By creating an account, you agree to BigBoss's 
                <span className='text-blue-600 cursor-pointer'> Condition of use </span> 
                and 
                <span className='text-blue-600 cursor-pointer'> Privacy Notice.</span>
              </p>

              <p className='text-xs text-gray-600 mt-4 cursor-pointer group'>
                <ArrowRight/> 
                <span className='text-blue-600 group-hover:text-orange-700 group-hover:underline underline-offset-1'>Need help?</span>
              </p>
            </div>

            <div className='w-full text-xs text-gray-600 mt-4 flex items-center'>
              <span className='w-1/4 h-[1px] bg-zinc-400 inline-flex'></span>
              <span className='w-2/4 text-center'>Already a customer?</span>
              <span className='w-1/4 h-[1px] bg-zinc-400 inline-flex'></span>
            </div>

            <Link to='/signin'>
              <button
              className='w-full py-1.5 mt-4 text-[.8em] font-normal rounded-sm bg-gradient-to-t from-slate-200 to-slate-100 hover:bg-gradient-to-b border border-zinc-400 active:border-yellow-800 active:shadow-martInput'
              >
                Sign in instead
              </button>
            </Link>
          </form>
        </div>

        <div className='w-full bg-gradient-to-t from-white via-white to-zinc-200 flex flex-col justify-center gap-4 items-center py-10'>
          <div className='flex items-center gap-6'>
            <p className='text-xs text-blue-600 hover:text-orange-600 hover:underline underline-offset-1 cursor-pointer duration-100'>
              Condition of Use
            </p>
            <span className='w-1.5 h-1.5 bg-zinc-400 rounded-full'></span>
            <p className='text-xs text-blue-600 hover:text-orange-600 hover:underline underline-offset-1 cursor-pointer duration-100'>
              Privacy Notice
            </p>
            <span className='w-1.5 h-1.5 bg-zinc-400 rounded-full'></span>
            <p className='text-xs text-blue-600 hover:text-orange-600 hover:underline underline-offset-1 cursor-pointer duration-100'>
              Terms & Conditions
            </p>
          </div>

          <p className='text-xs text-gray-600'>
            &copy;{currentYear} BigBoss.com, Inc. or its affiliates
          </p>

        </div>
    </div>
  )
}

export default SignUp