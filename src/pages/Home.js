import React from 'react'
import Banner from '../components/home/Banner'
import Products from '../components/home/Products'
import { useTheme } from '../context/ThemeContext'

const Home = () => {

  const { theme } = useTheme();

  return (
    <div>
        <Banner/>
        <div className={`${theme === 'dark' ? 'bg-mart_light' : ''} w-full -mt-20 py-10`}>
          <Products/>
        </div>
        
    </div>
  )
}

export default Home