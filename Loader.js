import { ColorRing } from  'react-loader-spinner';

const Loader = () => {
    <div className='flex justify-center items-center'>
        <ColorRing
            visible={true}
            height="50"
            width="50"
            ariaLabel="blocks-loading"
            wrapperStyle={{}}
            wrapperClass="blocks-wrapper"
            colors={['#b8c480', '#B2A3B5', '#F4442E', '#51E5FF', '#429EA6']}
        />
    </div>
};

export default Loader;